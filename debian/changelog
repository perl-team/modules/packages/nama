nama (1.307-1) UNRELEASED; urgency=medium

  PROBLEM:
  there is no Build.PL (or anything else) in the tarball

  TODO:
  check license: GPL-3 to either "perl_5" or "Artistic"

  * Import upstream version 1.307.
  * Drop debian/tests/pkg-perl/use-name. We now have META.{json,yml}.
  * Drop pod.patch, applied upstream.
  * Update spelling.patch.
  * Drop copyright statement about removed files.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Update build, test, and runtime dependencies.

 -- gregor herrmann <gregoa@debian.org>  Thu, 21 Dec 2023 21:03:13 +0100

nama (1.216-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 04:35:42 +0100

nama (1.216-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Import upstream version 1.216.
  * Refresh pod.patch.
  * Drop workaround-perl-stack-not-refcounted-crash.patch.
    Code was changed upstream.
  * Refresh spelling.patch.
  * Update years of packaging copyright.
  * Add new (build) dependencies.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.
  * Build-depend on libmodule-install-perl.
  * Fix hashbang in executable.

 -- gregor herrmann <gregoa@debian.org>  Sun, 23 Feb 2020 12:26:40 +0100

nama (1.208-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Joel Roth from Uploaders. Thanks for your work!

  [ Balint Reczey ]
  * Add patch to work around Perl crash. The construct used in nama
    triggers a segfault in Perl which surfaced recently in 5.23.3.
    (Closes: #839218)

  [ gregor herrmann ]
  * Add /me to Uploaders.
  * debian/control: use HTTPS in Homepage field.
  * Add a patch to fix spelling mistakes in the POD.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Mon, 27 Mar 2017 17:53:38 +0200

nama (1.208-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.208
  * Add d/u/metadata

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 11 Jan 2016 23:47:47 -0200

nama (1.207-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.207.
  * Drop spelling.patch, merged upstream.
  * Update pod.patch, partly applied upstream.

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Jan 2016 15:05:17 +0100

nama (1.204-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Damyan Ivanov ]
  * Imported Upstream version 1.109
  * upstream code licensing changed to GPL-3
  * update dependencies

  [ gregor herrmann ]
  * New upstream release 1.114.
  * Strip trailing slash from metacpan URLs.
  * Add new (build) dependencies.
  * Remove unnecessary version constraint from libanyevent-perl.
    Nothing older in the archive.
  * Declare compliance with Debian Policy 3.9.5.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Drop override_dh_auto_test, debhelper runs tests verbosely since
    9.20150501.
  * New upstream release 1.204.
    Fixes "ChainSetup fails rendering all sound operations useless"
    (Closes: #800913)
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.
  * Update GPL-3 blurb in debian/copyright.
  * Add patches to fix spelling and POD syntax glitches.

 -- gregor herrmann <gregoa@debian.org>  Tue, 29 Dec 2015 04:08:54 +0100

nama (1.078-2) unstable; urgency=low

  * Team upload
  * Fix long description formatting
  * Suggest libaudio-ecasound-perl
  * Depends on ecasound (nama fails to start if ecasound is not present)
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 29 Feb 2012 14:57:48 +0100

nama (1.078-1) unstable; urgency=low

  * New upstream release

 -- Joel Roth <joelz@pobox.com>  Thu, 18 Aug 2011 20:26:09 -1000

nama (1.077-1) unstable; urgency=low

  * New upstream release

 -- Joel Roth <joelz@pobox.com>  Fri, 12 Aug 2011 08:23:25 -1000

nama (1.076-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Joel Roth ]
  * New upstream release

 -- Joel Roth <joelz@pobox.com>  Mon, 08 Aug 2011 13:47:12 -1000

nama (1.073-1) unstable; urgency=low

  [ gregor herrmann ]
  * Add (build) dependency on procps (partly fixes #615510).
  * debian/copyright: update license stanzas.

  [ Joel Roth ]
  * New upstream release (closes: #615510, #597033).
    - track edits (non-destructive punch-in style recording)
    - track comments
    - version comments
    - project templates
    - autosave
    - support jack.plumbing and jack_connect for JACK client connections
    - user-defined commands (custom.pl)
    - initial support for Midish MIDI sequencer and filter
    - expanded test coverage
    - separate code into multiple modules

  [ Salvatore Bonaccorso ]
  * debian/control:
    - Add (Build-)Depends(Indep) on libfile-slurp-perl.
    - Remove libio-all-perl (Build-)Depends(-Indep).

  [ Joel Roth ]
  * New upstream release
    - fix bug with inserts
    - move entire man page to executable

  [ gregor herrmann ]
  * Remove debian/nama.links, the script has a manpage of its own now.
  * Add a patch to fix the whatis entry for the module's manpage.
  * Set Standards-Version to 3.9.2 (no changes).

 -- Joel Roth <joelz@pobox.com>  Sun, 17 Apr 2011 13:41:12 -1000

nama (1.064-6) unstable; urgency=low

  * Fix "Startup fails because socket creation fails"
    shell syntax for starting Ecasound now compatible
    with dash (Closes: #599693)

 -- Joel Roth <joelz@pobox.com>  Sun, 24 Oct 2010 23:39:19 -1000

nama (1.064-5) unstable; urgency=low

  * Fix "update_send_bus_cooked command fails due to illegal method
    call" - by correcting method call (Closes: #597757)

 -- Joel Roth <joelz@pobox.com>  Thu, 23 Sep 2010 21:51:52 -1000

nama (1.064-4) unstable; urgency=low

  * delete RIFF headers before configuring engine (closes: #596991)

 -- Joel Roth <joelz@pobox.com>  Tue, 21 Sep 2010 12:34:27 -1000

nama (1.064-3) unstable; urgency=low

  * patch in missing man page section "TEXT COMMANDS"

 -- Joel Roth <joelz@pobox.com>  Fri, 20 Aug 2010 09:58:51 -1000

nama (1.064-2) unstable; urgency=low

  * remove Recommends: line to enable migration

 -- Joel Roth <joelz@pobox.com>  Thu, 12 Aug 2010 10:07:54 -1000

nama (1.064-1) unstable; urgency=low

  * New upstream release

 -- Joel Roth <joelz@pobox.com>  Sun, 08 Aug 2010 20:47:48 -1000

nama (1.063-1) unstable; urgency=low

  * Team Upload.
  * New upstream release.
    + Test suite now passes without a controlling terminal. (Closes: #591166)
  * debian/control: Correct Vcs-* URL.
  * Make (build-)dep on perl unversioned as stable already has 5.10.
  * debian/copyright: Formatting changes for current DEP-5 proposal;
    refer to /usr/share/common-licenses/GPL-1.
  * Bump Standards-Version to 3.9.1.

 -- Ansgar Burchardt <ansgar@43-1.org>  Tue, 03 Aug 2010 14:18:35 +0900

nama (1.062-1) unstable; urgency=low

  * Initial Release (closes: #563299)

 -- Joel Roth <joelz@pobox.com>  Wed, 09 Jun 2010 08:28:18 -1000
